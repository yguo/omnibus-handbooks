FROM debian:jessie

# Install required packages
RUN apt-get update -q \
    && apt-get install -yq --no-install-recommends \
      vim \ 
      curl \
      wget \
      ca-certificates

RUN curl -s https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh | bash

RUN wget --content-disposition https://packages.gitlab.com/gitlab/nightly-builds/packages/ubuntu/xenial/gitlab-ee_11.9.1+rnightly.108069.b9883edd-0_amd64.deb/download.deb




